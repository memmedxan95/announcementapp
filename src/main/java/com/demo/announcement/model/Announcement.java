package com.demo.announcement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Entity
@Table(name = "announcements")
@Getter
@Setter
@RequiredArgsConstructor
public class Announcement implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "announcement_id", nullable = false)
    private Long id;

    @Column(name = "view_count")
    private Integer viewCount;

    @OneToOne(mappedBy ="announcement", cascade = CascadeType.ALL)
    private AnnouncementDetail announcementDetail;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

}