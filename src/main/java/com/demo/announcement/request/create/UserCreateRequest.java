package com.demo.announcement.request.create;


import com.demo.announcement.enums.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateRequest {


    private String name;
    private String surname;
    private String password;
    @Email
    private String email;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private String birthDate;
    @Enumerated(EnumType.STRING)
    private Gender gender;



}
