package com.demo.announcement.request.create;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementRequest {

    private String title;
    private String description;
    private Integer price;


}
