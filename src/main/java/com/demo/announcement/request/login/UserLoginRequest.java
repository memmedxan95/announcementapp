package com.demo.announcement.request.login;

import com.demo.announcement.model.Announcement;
import jakarta.persistence.Column;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginRequest {


    private String username;
    private String password;


}
