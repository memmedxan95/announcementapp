package com.demo.announcement.constant;

public class UserMessages {


    public static final String USER_NOT_FOUND = "User not found by id. ";
    public static final String USER_EXIST = "User exits this PIN code, you can not create. ";
    public static final String USER_UNATHORIZE = "You dont have permission. ";
    public static final String USER_SUCCES_LOGIN = "Login is succesfully. ";
    public static final String USER_FAILED_LOGIN = "Login is failed. ";
    public static final String USER_SUCCES_ACTIVATED = "User account is activated. ";
}
