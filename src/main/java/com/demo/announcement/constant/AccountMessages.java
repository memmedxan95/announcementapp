package com.demo.announcement.constant;

public class AccountMessages {


    public static final String ACCOUNT_NUMBER_IS_SAME = "Account numbers are the same. Please, check again. ";


    public static final String ACCOUNT_NUMBER_FROM_IS_DEACTIVE = "From account number is Deactive. ";
    public static final String ACCOUNT_NUMBER_TO_IS_DEACTIVE = "To account number is Deactive. ";
    public static final String ACCOUNT_NUMBER_IS_NOT_FOUND = "Account number is not found. ";
    public static final String BALANCE_IS_NOT_ENOUGH = "Balance is not enough for transfer. ";
}
