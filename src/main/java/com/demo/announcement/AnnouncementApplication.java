package com.demo.announcement;


import com.demo.announcement.mapper.AnnouncementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class AnnouncementApplication {



	public static void main(String[] args) {
		SpringApplication.run(AnnouncementApplication.class, args);
	}

}
