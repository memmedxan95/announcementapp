package com.demo.announcement.exception;

public class BadRequestException extends RuntimeException {
    public BadRequestException(String code, String message) {
        super(message);
    }
}