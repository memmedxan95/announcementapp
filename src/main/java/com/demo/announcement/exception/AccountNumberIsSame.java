package com.demo.announcement.exception;

public class AccountNumberIsSame extends  RuntimeException{

    public AccountNumberIsSame(String code, String message) {
        super(message);
    }

}
