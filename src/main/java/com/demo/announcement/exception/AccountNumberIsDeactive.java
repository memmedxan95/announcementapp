package com.demo.announcement.exception;

public class AccountNumberIsDeactive extends RuntimeException {
    public AccountNumberIsDeactive(String code, String message) {
        super(message);
    }
}
