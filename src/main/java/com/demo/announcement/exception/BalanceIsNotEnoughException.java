package com.demo.announcement.exception;

public class BalanceIsNotEnoughException extends RuntimeException{
    public BalanceIsNotEnoughException(String code, String message) {
        super(message);
    }
}
