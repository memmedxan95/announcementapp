package com.demo.announcement.exception.handler;



import com.demo.announcement.exception.UserNotFoundException;
import com.demo.announcement.exception.error.ErrorResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class ErrorHandler {

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(BAD_REQUEST)
    public ProblemDetail userNotFoundException(Exception exception) {
        log.info("userNotFoundException {}", exception.getMessage());
        return ProblemDetail.forStatusAndDetail(NOT_FOUND, exception.getMessage());
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handleMethodArgumentNot(HttpServletRequest request,
                                                 MethodArgumentNotValidException exception,
                                                 HttpServletResponse response) {
        ArrayList<String> errors = new ArrayList<>();
        exception.getBindingResult()
                .getFieldErrors()
                .stream()
                .forEach(fieldError -> errors.add(fieldError.getDefaultMessage()));

        return ErrorResponse.builder()
                .timestamp(LocalDate.now())
                .validationsMessages(errors)
                .statusCode(BAD_REQUEST.value())
                .path(request.getServletPath())
                .build();


    }


}


