package com.demo.announcement.exception;

public class AnnouncementNotFoundException extends RuntimeException {

    public AnnouncementNotFoundException (String code, String message){
        super(message);
    }
}
