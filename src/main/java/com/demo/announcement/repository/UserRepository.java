package com.demo.announcement.repository;


import com.demo.announcement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long>  {


    Optional<User> findByEmail (String email);




    @Query("SELECT u FROM users u WHERE u.username =:username AND u.password =:password")
    Optional<User> findUserWithUsernameAndPassword(String username, String password);



    @Modifying
    @Query("UPDATE users u SET u.enabled=TRUE WHERE u.id=:id")
    void updateUserStatusToActive(@Param("id") Long id);

}
