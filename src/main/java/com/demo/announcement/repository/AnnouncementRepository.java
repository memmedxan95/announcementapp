package com.demo.announcement.repository;

import com.demo.announcement.model.Announcement;
import com.demo.announcement.model.User;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;


@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Long> , JpaSpecificationExecutor<Announcement> {

       Optional< Announcement> findByUser(User user);


       @Query("SELECT a FROM Announcement a WHERE a.user = :user")
       Page<Announcement> findByUser(@Param("user") User user, Pageable pageable);

       Optional<Page<Announcement>> findAllByUserOrderByViewCountDesc(User user, Pageable pageable);


       @Query("select a from Announcement a order by a.viewCount desc")
       Optional<Page<Announcement>> findMostByOrderDesc(Pageable pageable);



}
