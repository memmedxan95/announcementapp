package com.demo.announcement.controller;

import com.demo.announcement.model.Announcement;
import com.demo.announcement.request.SearchCriteria;
import com.demo.announcement.request.create.AnnouncementRequest;
import com.demo.announcement.response.AnnouncementResponse;
import com.demo.announcement.service.AnnouncementServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/announcement")
public class AnnouncementController {

    private final AnnouncementServiceImpl announcementService;


    @PostMapping("/criteria")
    public ResponseEntity<List<Announcement>> findAllStudents(@RequestBody List<SearchCriteria> searchCriteriaList) {
        return ResponseEntity.ok(announcementService.findAllAnnouncement(searchCriteriaList));
    }

    @GetMapping("/all")
    public ResponseEntity<Page<Announcement>> getAnnouncementAll(@RequestParam(value = "pageSize") int pageSize,
                                                             @RequestParam(value = "pageNumber") int pageNumber,
                                                             @RequestParam(value = "pageSort") String[] pageSort) {
        return ResponseEntity.ok(announcementService.getAnnouncementAll(pageSize, pageNumber, pageSort));
    }

    @GetMapping("/allown")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public ResponseEntity<Page<Announcement>> getOwnAnnouncementAll(Pageable pageable, @RequestParam Long id ) {
        return ResponseEntity.ok(announcementService.getOwnAnnouncementAll(pageable, id));
    }

    @GetMapping("/ownbyid")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public ResponseEntity<AnnouncementResponse> getOwnAnnouncementById(@RequestParam Long userId,
                                                                       @RequestParam Long announcementId) {
        return ResponseEntity.ok(announcementService.getOwnAnnouncementById(userId, announcementId));
    }

    @GetMapping("/mostownbyid")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public ResponseEntity<Page<AnnouncementResponse>> getMostViewedOwnAnnouncementById(@RequestParam Long userId,
                                                                                       Pageable pageable) {
        return ResponseEntity.ok(announcementService.getMostViewedOwnAnnouncementById(userId, pageable));
    }


    @GetMapping("/mostall")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public ResponseEntity<Page<AnnouncementResponse>> getMostViewedAnnouncement(Pageable pageable) {
        return ResponseEntity.ok(announcementService.getMostViewedAnnouncement(pageable));
    }



    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void createAnnouncement(@RequestBody AnnouncementRequest announcementRequest, Long id) {
        announcementService.createAnnouncement(announcementRequest, id);
    }

    @PutMapping("/update")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public void updateAnnouncement(@RequestBody AnnouncementRequest announcementRequest, Long id) {
        announcementService.updateAnnouncement(announcementRequest, id);
    }

    @DeleteMapping("/delete")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    public void deleteAnnouncement(Long announcementId, Long userId) {
        announcementService.deleteAnnouncement(announcementId, userId);
    }











}
