package com.demo.announcement.configuration;


import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.http.HttpMethod.*;

@Configuration
@RequiredArgsConstructor
@EnableMethodSecurity
public class SecurityConfig {

    private final JwtAuthFilter jwtAuthFilter;
//    private final TestFilter testFilter;
    private final CustomEntryPoint customEntryPoint;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.csrf(AbstractHttpConfigurer::disable);
        http.authorizeHttpRequests((authz) ->
                authz.requestMatchers(POST, "/v1/users/registration", "/v1/users/resendVerificationCode", "/v1/announcement/create").permitAll()
                        .requestMatchers(PUT,"/v1/users/authenticate").permitAll()
                        .requestMatchers(GET,"/v1/users/login" , "v1/announcement/all").permitAll()
                        .requestMatchers(PUT, "/v1/announcement/update").hasAnyRole("ADMIN","USER")
                        .requestMatchers(DELETE, "/v1/announcement/delete").hasAnyRole("ADMIN","USER")
                        .requestMatchers(GET, "/v1/announcement/*").hasAnyRole("ADMIN","USER")
                        .anyRequest().authenticated()
                );
//        http.httpBasic(basic -> basic.authenticationEntryPoint(customEntryPoint));
        http.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);
        http.exceptionHandling(Customizer.withDefaults());
        return http.build();

    }


//    @Bean
//    public WebSecurityCustomizer webSecurityCustomizer() {
//        return (web) -> web.ignoring().requestMatchers("/v2/api-docs",
//                "/swagger-resources",
//                "/swagger-resources/**",
//                "/configuration/ui",
//                "/configuration/security",
//                "/swagger-ui.html",
//                "/webjars/**",
//                // -- Swagger UI v3 (OpenAPI)
//                "/v3/api-docs/**",
//                "/swagger-ui/**");
//    }





//    @Bean
//    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
//        return configuration.getAuthenticationManager();
//    }


}
