package com.demo.announcement.configuration;


import com.demo.announcement.exception.error.ErrorResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;


@Component
@Slf4j
@RequiredArgsConstructor
public class TestFilter  extends OncePerRequestFilter {

    public static final String BEARER = "Bearer ";
    private final JwtService jwtService;



    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String token = request.getHeader("Authorization");

        try {


            if(request.getRequestURI().equals("/v1/users/error")){
//                throw new RuntimeException("Path incorrect");
            }



        } catch (Exception e) {

            e.printStackTrace();

            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setStatusCode(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value());
            errorResponse.setMessage(e.getMessage());

//            response.getWriter().print(convertExceptionToObject(errorResponse));
//            response.setStatus(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value());
//            return;
        }


        filterChain.doFilter(request, response);
    }

    public String convertExceptionToObject(Object object) throws JsonProcessingException {
        if (object == null) return null;
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

}
