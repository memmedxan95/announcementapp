package com.demo.announcement.mapper;

import com.demo.announcement.model.Announcement;
import com.demo.announcement.model.AnnouncementDetail;
import com.demo.announcement.request.create.AnnouncementRequest;
import com.demo.announcement.response.AnnouncementResponse;
import org.mapstruct.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.List;


@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AnnouncementMapper {

//    public static final AnnouncementMapper INSTANCE = Mappers.getMapper(AnnouncementMapper.class);

    @Mapping(target = "announcementDetail.title", source = "title" )
    @Mapping(target = "announcementDetail.description", source = "description")
    @Mapping(target = "announcementDetail.price", source = "price")
    Announcement requestToEntity(AnnouncementRequest announcementRequest);


    @Mapping(target = "title", source = "announcementDetail.title"  )
    @Mapping(target = "description", source = "announcementDetail.description")
    @Mapping(target = "price", source = "announcementDetail.price" )
    @Mapping(target = "viewCount", source = "viewCount")
    AnnouncementResponse announcementToResponse(Announcement announcement);


//    @Mapping(target = "title", source = "announcementDetail.title"  )
//    @Mapping(target = "description", source = "announcementDetail.description")
//    @Mapping(target = "price", source = "announcementDetail.price" )
//    @Mapping(target = "viewCount", source = "viewCount")
//    List<AnnouncementResponse> announcementListToResponseList(List<Announcement> announcementList);
}

