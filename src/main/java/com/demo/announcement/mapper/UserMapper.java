package com.demo.announcement.mapper;



import com.demo.announcement.model.User;
import com.demo.announcement.request.create.UserCreateRequest;
import com.demo.announcement.response.UserCreateResponse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    User createRequestToEntity (UserCreateRequest userCreateRequest);
//    User deleteRequestToEntity(UserDeleteRequest userDeleteRequest);

    UserCreateResponse entityToResponseCreate(User user);

//    UserReadResponse entityToResponseRead(User user);
//    UserUpdateResponse entityToResponseUpdate(User user);

//    User updateRequestToEntity(UserUpdateRequest userUpdateRequest);



}
