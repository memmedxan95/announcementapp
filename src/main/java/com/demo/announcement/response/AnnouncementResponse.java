package com.demo.announcement.response;

import jakarta.persistence.Column;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AnnouncementResponse {

    private Integer viewCount;
    private String title;
    private String description;
    private double price;


}
