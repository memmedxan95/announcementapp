package com.demo.announcement.service;

import com.demo.announcement.model.Announcement;
import com.demo.announcement.request.SearchCriteria;
import com.demo.announcement.request.create.AnnouncementRequest;
import com.demo.announcement.response.AnnouncementResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;



public interface AnnouncementService {

    public List<Announcement> findAllAnnouncement(List<SearchCriteria> searchCriteriaList);



    public Page<Announcement> getAnnouncementAll(int pageSize, int pageNumber, String[] pageSort);

    public void createAnnouncement(AnnouncementRequest announcementRequest, Long id);

    void updateAnnouncement(AnnouncementRequest announcementRequest, Long id);

    void deleteAnnouncement(Long announcementId, Long userId);

    Page<Announcement> getOwnAnnouncementAll(Pageable pageable, Long id);


    AnnouncementResponse getOwnAnnouncementById(Long userId, Long announcementId);

    Page<AnnouncementResponse> getMostViewedOwnAnnouncementById(Long userId,Pageable pageable);

    Page<AnnouncementResponse> getMostViewedAnnouncement(Pageable pageable);
}
