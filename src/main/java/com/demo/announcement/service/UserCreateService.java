package com.demo.announcement.service;

import com.demo.announcement.request.create.AuthenticateRequest;
import com.demo.announcement.request.create.JwtDto;
import com.demo.announcement.request.create.UserCreateRequest;
import com.demo.announcement.response.UserCreateResponse;


public interface UserCreateService {


    UserCreateResponse createUser(UserCreateRequest userCreateRequest);

    void activate(String email, Integer verificationCode);

    void resendVerificationCode(String email);

    JwtDto authenticate(AuthenticateRequest authenticateRequest);

}
